#include <iostream>
#include <ControllerBattery.h>

#include <ShellInterface.h>
int main(int argc, char const *argv[]){
	ControllerBattery ct;

	ShellInterface<ControllerBattery> s(argc, argv);
	s.setAppName("AssistantBattery");
	s.setAppVersion("v1.0.0");
	s.manDescription("This assistant manages battery level. It will warn you when you should disconnect or connect it to electricity");
	
	s.manSubTitle("Options:");
	s.manFlag("-bi", "Batery Info");
	s.manFlag("-ba", "Batery Alert");

	unsigned int n = 2;
	const DOLinking<ControllerBattery> d[n] = {
		{"-bi", &ControllerBattery::SpeakInfo},
		{"-ba", &ControllerBattery::SpeakInfoAlert}
	};
	s.run(d, n);
	return 0;
}
/*

const DOLinking<ControllerBattery> d[2] = {
	{"bi", &ControllerBattery::SpeakInfo},
	{"ba", &ControllerBattery::SpeakInfoAlert}
};

void (ControllerBattery::*action)(void) = &ControllerBattery::SpeakInfo;
ControllerBattery *t = new ControllerBattery();
(t->*action)();

*/
