#include <ControllerBattery.h>

ControllerBattery::ControllerBattery(void){
	getACPI();
}
ControllerBattery::~ControllerBattery(void){

}
string ControllerBattery::cleanACPI(bool st = false){
	string snew = str_replace("remaining","", acpi);
	snew = str_replace("Battery 0: ","", snew);
	snew = str_replace("Discharging, ","Descargandose,", snew);
	snew = str_replace("Charging, ","Cargandose,", snew);
	snew = str_replace("until charged","", snew);
	snew = str_replace("remaining","", snew);
	snew = str_replace("Unknown","", snew);
	snew = str_replace("Full, ","Lleno,", snew);
	if(st){
		snew = str_replace("Descargandose,","", snew);
		snew = str_replace("Cargandose,","", snew);
		snew = str_replace("Lleno,","", snew);
	}
	return snew;
}
void ControllerBattery::getACPI(void){
	ShellCommand sc;
	acpi = sc.GetOutSystem("acpi");	
}
string ControllerBattery::getPercentage(void){
	string snew = cleanACPI(true);
	int per = strpos(",",snew);
	if(per == -1) 	snew = snew;
	else 			snew = substr(0, per, snew);
	
	return snew;
}
string ControllerBattery::getTime(void){
	string snew = cleanACPI(true);
	int t = strpos(",",snew);
	if(t == -1){
		snew = "";
	} else {
		snew = substr(t+2, t+7, snew);
		if(strpos("%", snew) != -1){
			snew = "";
		} else {
			t = strpos(":", snew);
			int a = atoi(substr(0, t, snew).c_str());
			string b = substr(t+1, 5, snew);
			snew = " ";
			if (a != 0){
				b[1] = '0'; 
				snew += (a == 1)? "una": to_string(a); 	
				snew += (a != 0)? (a == 1)? " hora": " horas":"";
			}
			if(b != "00"){
				snew += " con ";
				snew += b;
			}
			snew += (b != "00")? (b == "01")? " minuto": " minutos":"";
		}
	}
	return snew;
}
string ControllerBattery::getAction(void){
	string snew = cleanACPI();
	snew = str_replace("andose","ue", snew);
	int t = strpos(",",snew);
	snew = substr(0, t, snew);
	return snew;
}

void ControllerBattery::SpeakInfo(void){
	string action     = getAction();
	string percentage = getPercentage();
	string time       = getTime();
	
	string str;
	str = "Estoy al ";
	str += percentage;
	str += " de batería";

	if (action != "Lleno"){
		str += ", Faltan";
		str += time;
		str += " para que se ";
		str += action;
		str += " completamente";
	} else {
		str += ", estoy completamente lleno";
	}
	DOSpeak(str.c_str());
}
void ControllerBattery::SpeakInfoAlert(void){
	string str;
	int percentage = atoi(str_replace("%","",getPercentage()).c_str());
	int action =  -1;
	string act = getAction();
	if(act == "Lleno") 				action = 0;
	else if(act == "Descargue")  	action = 1;
	
	if(action == 0 && percentage == 100){
		str += "Ya estoy completamente cargado. Desconéctame";
	} else if(action == 1 && percentage < 15){
		str += "Tengo ";
		str += percentage;
		str += " de batería, Conecta la batería";
	}
	DOSpeakLoudly(str.c_str());
}