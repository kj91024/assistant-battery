#include <File.h>
File::File(){
}
File::File(string path){
	dir = path;
}
File::~File(){
	close();
}
string File::read(){
	char c;
	string s;
	this->f = fopen(dir.c_str(),"r");
	while((c = fgetc(this->f)) != EOF)
		s += c;
	return s;
}
string File::read(const char *name){
	char c;
	string s = "";

	if(isFile(name)){
		this->f = fopen(name,"r");
		while((c = fgetc(this->f)) != EOF){
			s += c;
		}
	} else {
		cout << "ERROR: The file is not exist" << endl;
	}
	return s;
}
bool File::isFile(const char *fp){
	return (fopen(fp,"r"))? true : false ;
}
/*void files::write(const char *fp, string text){
	string msn = ZZtoString(text);
	this->fp = fopen(fp,"w+");
	for(int i = 0;i<msn.size();i++)
		fputc(msn[i],this->fp);
}*/
void File::close(void){
	if(this->f) fclose(this->f);
}