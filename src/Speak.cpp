#include <Speak.h>
Speak::Speak(){
	cmd = "espeak -ves+f3 ";
}
Speak::~Speak(){

}
int Speak::getVolume(void){
	string snew = GetOutSystem("amixer");
	int a = strpos("[",snew)+1;
	int b = strpos("]",snew);
	snew  = substr(a, b, snew);
	snew  = str_replace("%", "", snew);
	return atoi(snew.c_str());
}
void Speak::setVolume(int n){
	string command = "amixer -D pulse sset Master ";
	command += to_string(n);
	command += "%";
	GetOutSystem(command.c_str());
}
void Speak::DOSpeak(const char* s){
	string command = cmd;
	command += "\"";
	command += s;
	command += "\"";
	GetOutSystem(command.c_str());
}
void Speak::DOSpeakLoudly(const char* s){
	string sn = s;
	if(sn != ""){
		int c_volume = getVolume();
		setVolume(100);
		DOSpeak(s);
		setVolume(c_volume);
	}
}