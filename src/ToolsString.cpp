#include <ToolsString.h>
string str_replace(const char* target, const char* replace, string s){
	string ns;
	string t = target;
	string r = replace;
	int nt = t.size();
	int nr = r.size();

	for (int i = 0; i < s.size(); ++i){
		int j = 0;
		for (; j < nt && s[i] == t[j]; ++j, ++i)
			if(j+1 < nt && s[i+1] != t[j+1]) break;
		if(nt == j){
			if(j > 0) --i;
			ns += r;
		} else ns += s[i];
	}
	return ns;
}
int strpos(const char* target, string s){
	string t = target;
	int nt = t.size();
	
	for (int i = 0; i < s.size(); ++i){
		int j = 0;
		for (; j < nt && s[i] == t[j]; ++j, ++i);
		if(nt == j)	return (i-j);
		if(j > 0) --i;
	}
	return -1;
}
string substr(int a, int b, string s){
	string ns;
	for (; a < b; ++a) ns += s[a];
	return ns;
}
string addstr(int a, int b, const char* replace, string s){
	string ns;
	for (int i = 0; i < a; ++i) 		ns += s[a];
	ns += replace;
	for (int i = b; i < s.size(); ++i) 	ns += s[a];
	return ns;
}