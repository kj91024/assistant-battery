#include <ShellCommand.h>
#include <File.h>

ShellCommand::ShellCommand(){
	filename = "outc.txt";
	f = new File();
}
ShellCommand::~ShellCommand(){
	delete f;
}
void ShellCommand::ExecCommand(const char *s, bool st){
	string command = s;
	 
	if(st){
		command += ">";
		command += filename; 
	}
	system(command.c_str());
}
string ShellCommand::GetOutSystem(const char *s){
	ExecCommand(s, true);
	string data = GetFile();
	DeleteFile();
	return data;
}
void ShellCommand::DeleteFile(void){
	string command = "rm -rf ";
	command += filename;
	ExecCommand(command.c_str());
}
string ShellCommand::GetFile(void){
	return f->read(filename.c_str());
}
