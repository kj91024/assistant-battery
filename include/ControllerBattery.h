#pragma once
#include <iostream>
#include <cstdlib>
#include <ShellCommand.h>
#include <ToolsString.h>
#include <Speak.h>
using namespace std;

class ControllerBattery: public Speak{
	string acpi;
public:
	ControllerBattery(void);
	~ControllerBattery(void);
	//getStatus();
	void SpeakInfo(void);
	void SpeakInfoAlert(void);
private:
	string getPercentage(void);
	void getACPI(void);
	string getTime(void);
	string getAction(void);
	string cleanACPI(bool);
};