#pragma once
#include <iostream>

template <typename T>
struct DOLinking{
	std::string request;
	void (T::*action)(void);
};

template <typename T>
class ShellInterface{
	std::string manual;
	std::string _appname, _version;
	bool _sttitle;
	bool _stdefault;
public:
	ShellInterface(int , char const **);
	~ShellInterface(void);
	void run(const DOLinking<T> *, unsigned int);
	void setAppName(std::string);
	void setAppVersion(std::string);

	void actionDefault(void);
	//Setting manual
	void manDescription(std::string);
	void manSubTitle(std::string);
	void manFlag(std::string);
	void manFlag(std::string, std::string);
	void manFlagDescription(std::string);
private:
	bool _st;
	int argc;
	char const **argv;
	const DOLinking<T> *_da;

	void manTitle(void);
	void getInfo(void);
	std::string getName(void);
	void AppHelp(void);
	void LinkActions(const DOLinking<T> *);
	void ResponseAction(unsigned int);
};

template <typename T>
ShellInterface<T>::ShellInterface(int argc, char const *argv[])
:argc(argc), argv(argv){
	_stdefault = true;
	_sttitle = false;
	_appname = getName();
	_version = "v0.0.0";
	manual = "";

	manTitle();
}
template <typename T>
ShellInterface<T>::~ShellInterface(void){
}
template <typename T>
std::string ShellInterface<T>::getName(void){
	std::string s = argv[0];
	if(s[0] == '.' && s[1] == '/'){
		std::string ns;
		for( int i=2;i < s.size() and s[i] != ' '; ns+=s[i], i++ );
		return ns;
	}
	return s;
}
template <typename T>
void ShellInterface<T>::getInfo(void){
	std::cout << manual << std::endl;
}
template <typename T>
void ShellInterface<T>::actionDefault(void){
	_stdefault = false;
}
template <typename T>
void ShellInterface<T>::AppHelp(void){
	if(_stdefault)
		if(argc <= 1)
			getInfo();
}
template <typename T>
void ShellInterface<T>::ResponseAction(unsigned int dan){
	if (argc > 1){
		for(int i = 1; argv[i]; i++){
			std::string q = argv[i];
			int j = 0;
			DOLinking<T> da = _da[j];
			while(j < dan){
				if(da.request == q){
					T *t = new T();
					(t->*da.action)();
					break;
				}
				da = _da[++j];	
			}
		}
	}
}
template <typename T>
void ShellInterface<T>::run(const DOLinking<T> *da, unsigned int dan){
	AppHelp();
	LinkActions(da);
	ResponseAction(dan);
}
template <typename T>
void ShellInterface<T>::LinkActions(const DOLinking<T> *da){
	this->_da = da;
}
template <typename T>
void ShellInterface<T>::setAppVersion(std::string s)	{ _version = s; manual = ""; manTitle();}
template <typename T>
void ShellInterface<T>::setAppName(std::string s)	{ _appname = s; manual = ""; manTitle();}

template <typename T>
void ShellInterface<T>::manTitle(void){
	manual += _appname;
	manual += " ";
	manual += _version;
	manual += "\n";
}
template <typename T>
void ShellInterface<T>::manDescription(std::string s){
	manual += s;
	manual += "\n";
}
template <typename T>
void ShellInterface<T>::manSubTitle(std::string s){
	manual += "\n";
	manual += "  ";
	manual += s;
	manual += "\n";
	manual += "   ";
}
template <typename T>
void ShellInterface<T>::manFlag(std::string s){
	manual += s;
	manual += " ";
}
template <typename T>
void ShellInterface<T>::manFlagDescription(std::string s){
	manual += "\t";
	manual += s;
	manual += "\n";
	manual += "   ";
	//cout << s << endl;
}
template <typename T>
void ShellInterface<T>::manFlag(std::string s, std::string c){
	manual += s;
	manFlagDescription(c);
}