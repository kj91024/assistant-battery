#pragma once
#include <iostream>
using namespace std;
class File{
	FILE *f;
	string pathname;
	string dir;
public:
	File();
	File(string);
	~File();
	string read();
	string read(const char *);
	void close(void);
	bool isFile(const char *fp);
};