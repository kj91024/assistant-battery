#pragma once
#include <iostream>
#include "ToolsString.h"
#include "ShellCommand.h"
class Speak:public ShellCommand{
	string cmd;
	int getVolume(void);
	void setVolume(int n);
public:
	Speak();
	~Speak();
	void DOSpeak(const char*);
	void DOSpeakLoudly(const char*);
};