#pragma once
#include <iostream>
#include "File.h"

class ShellCommand{
	string filename;
	File *f;
public:
	ShellCommand();
	~ShellCommand();
	void ExecCommand(const char *, bool = false);
	string GetOutSystem(const char *);
private:
	string GetFile(void);
	void DeleteFile(void);
};