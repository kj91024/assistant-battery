# Assistant Battery (v1.0.0)
# This project is about an assistant for laptops, because 
# I forgot put electricity, then create this program for 
# speak me. I forget tell you, you need put the comand: 
# crontab  
 
# Powered by QWE.
     
#[*]        Version: 1.0.0
#[*]           Type: Comand Line
#[*]    Command Run: abattery
#[*]   Dependencies: espeak
#[*]         Author: Kevin Sanchez
#[*]   Author Email: kj91024
#[*]        Webpage: http://kj91024.com                 

# WARNING: If you don't know setting this file, then do not 
# edit anything, because you can to spoil this file.

## Program Basic Info
VERSION 	 := 1.0.0
NAME 		 := Assistant Battery
COMMAND_RUN  := abattery
FILEMAIN 	 := main.cpp

## Compiler of Program
CC           := g++
CFLAG 		 := -Wall

## Directories for project 
DIR_INCLUDE  := include/
DIR_SOURCE 	 := src/
DIR_LIBRARY  := lib/
DIR_ASSETS   := assets/
DIR_RELEASE  := release/

DIR_INSTALL  := /usr/local

OBJMAIN 	 := $(subst .cpp,.o,$(FILEMAIN))
SOURCES		 := $(wildcard $(DIR_SOURCE)*.cpp)
SRCOBJECTS	 := $(subst .cpp,.o,$(SOURCES))
BUILDOBJECTS := $(subst $(DIR_SOURCE),,$(SOURCES))
BUILDOBJECTS := $(subst .cpp,.o,$(BUILDOBJECTS))
OBJECTS		 := $(subst $(DIR_SOURCE),,$(SRCOBJECTS))

$(COMMAND_RUN): $(OBJECTS) $(OBJMAIN)
	@echo
	$(CC) -o $(COMMAND_RUN) $(OBJMAIN) $(BUILDOBJECTS) -I$(DIR_INCLUDE)
	@echo
	@echo "Process complete."

$(OBJMAIN): $(FILEMAIN)
	$(CC) -c $^ -I$(DIR_INCLUDE)
	
%.o: $(DIR_SOURCE)%.cpp
	$(CC) -c $^ -I$(DIR_INCLUDE)
	
clean:
	rm -rf *.o
	rm -rf $(COMMAND_RUN)
	rm -rf $(DIR_RELEASE)
	@echo 
	@echo "Cleaning complete."

install:
	@echo 
	@echo "Install $(NAME) v$(VERSION)"
	@echo "You can run $(NAME) with this command $(COMMAND_RUN)"
	@echo "If you wish to install it system wide, type 'sudo make install'"
	@echo "Uninstall with 'sudo make uninstall'"
	@echo 
	@echo "We create folder release where we put all need for execute the project"
	rm -fr $(DIR_RELEASE)
	mkdir $(DIR_RELEASE) $(DIR_RELEASE)bin
	cp $(COMMAND_RUN) $(DIR_RELEASE)bin/$(COMMAND_RUN)
	#cp $(DIR_LIBRARY) $(DIR_RELEASE)$(DIR_LIBRARY)
	#cp $(DIR_ASSETS) $(DIR_RELEASE)$(DIR_ASSETS)
	rm -rf $(DIR_INSTALL)/share/$(COMMAND_RUN)
	mkdir -p $(DIR_INSTALL)/share/$(COMMAND_RUN)
	cp -av $(DIR_RELEASE)* $(DIR_INSTALL)/share/$(COMMAND_RUN)/
	mkdir -p $(DIR_INSTALL)/bin

	ln -sf $(DIR_INSTALL)/share/$(COMMAND_RUN)/bin/$(COMMAND_RUN) $(DIR_INSTALL)/bin/
	
	@echo 
	@echo "Install complete. Type '$(COMMAND_RUN)' to run."

uninstall:
	rm -rf $(DIR_INSTALL)/share/$(COMMAND_RUN)
	rm -rf $(DIR_INSTALL)/bin/$(COMMAND_RUN)
	
	@echo
	@echo "Uninstall complete."

# Powered by QWE.