# Assistant Battery
This project is about an assistant for laptops, because 
I forgot put electricity, then create this program for 
speak me. I forget tell you, you need put the comand: 
crontab

## Details
* Version: 1.0.0
* Type: Comand Line
* Command Run: abattery
* Dependencies: espeak
* Author: Kevin Sanchez
* Author Email: kj91024
* Webpage: [http://kj91024.com](http://kj91024.com)

Powered with [QWE](https://gitlab.com/kj91024/QWE)

## Copyright and License
Assistant Battery is licensed under the Apache License, Version 2.0. See [LICENSE](https://github.com/moby/moby/blob/master/LICENSE) for the full
license text.

---
[Donate to support QWE](#donate)